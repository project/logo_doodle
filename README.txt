README.txt
==========

The Logo Doodle module allows site administrators to have variations of the site
logo for special occasions.

This module allows you to upload a different logo along with a time-frame during
which it will be shown on the site.

There is also an integration with the Birthdays.module to allow site
administrators to show a special birthday logo on the site on the site users
birthday.

Usage:
 - Enable the module as usual.
 - To schedule a doodle for a particular, create a new node of type
    'logo doodle', upload the logo and set the start and end dates.
 - To configure a doodle to be shown on a site user's birthday, enable the
    Birthdays module, attach a birthday field to the user and configure the
    doodle at admin/config/system/logo_doodle
